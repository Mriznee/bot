import pymysql
import sys
from config import Config
import json
import logging

class Db:
    
    @staticmethod
    def dbconn():
          return pymysql.connect(
              host = Config.Db_Host, 
              user = Config.Db_User, 
              passwd =Config.DB_Password, 
              db = Config.Db_Name , 
              charset="utf8mb4",
              cursorclass=pymysql.cursors.DictCursor)   

    @staticmethod
    def dbupdate(query):
        dbquery = None
        try:
            dbquery = Db.dbconn()
            query_cursor = dbquery.cursor()
            query_cursor.execute(query)
            dbquery.commit()
            result = query_cursor.lastrowid
            return {"result":result}
        except pymysql.InternalError  as error:
            data = "Error because of -: %s"%(error)
            dbquery.rollback()
            return data
        finally:
            query_cursor.close()
            dbquery.close()
            

    @staticmethod
    def dbget(query):
        dbquery = None
        dbquery = Db.dbconn()
        try:
            query_cursor = dbquery.cursor()
            query_cursor.execute(query)
            headers=[x[0] for x in query_cursor.description]
            result = query_cursor.fetchall()
            return {"result":result}            
        except pymysql.InternalError as error:
            data = "Error because of -: %s"%(error)
            return data
        finally:
            query_cursor.close()
            dbquery.close()
    
    @staticmethod
    def dbget_one(query):
        dbquery = None
        dbquery = Db.dbconn()
        try:
            query_cursor = dbquery.cursor()
            query_cursor.execute(query)
            headers=[x[0] for x in query_cursor.description]
            result = query_cursor.fetchone()
            return {"result":result}  
        except pymysql.InternalError  as error:
            data = "Error because of -: %s"%(error)
            return data
        finally:
            query_cursor.close()
            dbquery.close()

   
   




   



        

   