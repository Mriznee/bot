from db import Db
import json
import logging


class BaseModel:
    tablename:str = ''
    rowid:int = 0
    feilds:list = None
    params:list = None
    data:list = None
    
    def __init__ (self):
        pass
    
    def tablesetup(self):
        pass
    

    def getOne(self):
        query = BaseModel.getQueryBuilder(self.tablename, self.rowid, self.feilds, self.params)
        data = Db.dbget_one(query)
        result = BaseModel.response_json(data) 
        return result

    
    def getAll(self):
        query = BaseModel.getQueryBuilder(self.tablename,self.feilds,self.params)
        data = Db.dbget(query)
        result = BaseModel.response_json(data) 
        return result

    def getQueryBuilder(self):  
        if self.rowid != None and self.tablename !=None:
            query ="SELECT * FROM `%s` WHERE `id` = %d"%(self.tablename, self.rowid)
            return query  
        elif  self.params ==  None and self.feilds == None and self.tablename != None:
            query ="SELECT * FROM `%s` LIMIT %d"%(self.tablename, 10)
            return query
        elif self.params ==  None and self.feilds != None and self.tablename != None:
            str_feilds = ', '.join(self.feilds)
            query ="SELECT %s FROM `%s`"%(str_feilds, self.tablename)
            return query
        elif self.params !=  None and self.feilds != None and self.tablename != None:
            str_feilds = ', '.join(self.feilds)
            query ="SELECT %s FROM `%s`  %s "%(str_feilds, self.tablename, self.params)
            return query
        elif feilds ==  None and params != None and tablename != None:
            query ="SELECT * FROM `%s` %s"%(self.tablename, self.params)
            return query
        else:
            query ="Error"
            return query

    def create(self):
        query = BaseModel.createQueryBuilder(self.tablename, self.feilds, self.data)
        result = Db.dbupdate(query)
        return result

    def createQueryBuilder(self):
        vdata =''
        process_values =''
        if  self.data !=  None and self.feilds != None and self.tablename != None:
            str_feilds = ', '.join(self.feilds)
            for i in self.data:
                vdata += "'%s',"%(i)
            count =len(vdata)
            process_values = vdata[0:count-1]
            query ="INSERT INTO `%s`(%s) VALUES (%s)"%(self.tablename, str_feilds ,process_values)
            return query
        else:
            query ="Error"
            return query
    
    def update(self):
        query = BaseModel.updateQueryBuilder(self.tablename, self.feilds, self.data, self.rowid)    
        data = Db.dbupdate(query)
        return data

    def updateQueryBuilder(self):
        if  self.data !=  None and self.feilds != None and self.tablename != None and self.rowid != None:
            setdata = BaseModel.setData(self.feilds , self.data)                
            query ="UPDATE %s SET %s WHERE id=%d"%(self.tablename, setdata, self.rowid)
            return query
        else:
            query ="Error"
            return query

    def setData( self):
        process_data = ''
        newdata =''
        listLen = len(self.feilds)
        for x in range(0,listLen, 1):
            newdata = newdata + (feilds[x]+ "="+ "'" +data[x] +"',")
            process_data =  process_data +(newdata)
        count =len(process_data)
        process_data = process_data[0:count-1]
        return process_data


    def deleteRow(self):
        query = BaseModel.deleteQueryBuilder(self.tablename, self.rowid)
        result = Db.dbupdate(query)
        return result

    def deleteQueryBuilder(self):
        if  self.rowid !=  None and  self.tablename != None:             
            query ="DELETE FROM %s WHERE id = %d"%(self.tablename, self.rowid)
            return query
        else:
            query ="Error"
            return query

    def response_json(data):
        if type(data) is int:
            return data
        else:
            return data


    @staticmethod
    def findRow(id:int=0, param:str=None):
        pass