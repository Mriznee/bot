import time,datetime
import numpy as np
import os
import random
import string
import nltk
import json
import io
import logging
from word import word_token
from sentence import sentence_tokein

from basemodel import BaseModel

from nltk.stem import WordNetLemmatizer
from nltk.tokenize import sent_tokenize

from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.chat.util import reflections

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


nltk.download('popular', quiet=True)
nltk.download('punkt')
nltk.download('wordnet')


class BotResponse:
    
    GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up","hey",)
    GREETING_RESPONSES = ["hi,You can ask anything about Aasandha", "hey,You can ask anything about Aasandha", "hi there, You can ask anything about Aasandha", "hello, You can ask anything about aasandha", "You can ask anything about aasandha, How may I help you"]
    
    BYE_INPUT = ["bye", "See ya", "TC"]
    BYE_RESPONSES = ['Good bye', 'Take care','See ya']
    
    THANKYOU_INPUT = ['thanks' ,'thank you very much', 'thank you']
    THANKYOU_RESPONSES =[ 'Youre welcome', 'Don’t mention it', 'My pleasure', 'Anytime', 'It was the least I could do', 'Glad to help']
    
    
    
    raw = None
    sent_tokens = sentence_tokein
    word_tokens = word_token
    DataAnalysis = None
    robo_response = ''
    sentimentData = None
    
    now = datetime.datetime.now()    
    lemmer = nltk.stem.WordNetLemmatizer()
    sid = SentimentIntensityAnalyzer()
    remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)
    
    
    def __init__ (self):
        # logging.basicConfig(filename="error.log", level=logging.DEBUG)
        # self.open_file()
        # self.sent_tokens = nltk.sent_tokenize(self.raw) 
        # self.word_tokens = nltk.word_tokenize(self.raw)
        # self.data_json()
        pass
    
    def data_json(self):
        with open('aasandha.txt','r', encoding='utf8', errors ='ignore') as fin:
            raw = fin.read().lower()
        self.sent_tokens = nltk.sent_tokenize(raw)
        self.word_tokens = nltk.word_tokenize(raw)
        # data = self.WordCounter(word_tokens)
        with open('sentence.py', 'w') as outfile:
            json.dump(self.sent_tokens, outfile)
        with open('word.py', 'w') as outfile:
            json.dump(self.word_tokens, outfile)
    
    
    def sentiment_analysiser(self,data):        
        self.sentimentData= self.sid.polarity_scores(data)
 
    
    def LemTokens(self, tokens):
        return [self.lemmer.lemmatize(token) for token in tokens]
    
    def LemNormalize(self, text):
        return LemTokens(nltk.word_tokenize(text.lower().translate(self.remove_punct_dict)))
    

    def greeting(self,sentence): 
        for word in sentence.split():
            if word.lower() in self.GREETING_INPUTS:
                return random.choice(self.GREETING_RESPONSES)
            
    def generating_thankyou(self):
        return random.choice(self.THANKYOU_RESPONSES)
         
            
    def generating_bye(self):
        return random.choice(self.BYE_RESPONSES)
    
    def WordCounter(self, data):
        for word in data:
            if word in DataAnalysis.split_data:
                self.DataAnalysis.split_data[word] += 1
            else:
                self.DataAnalysis.split_data[word] = 1
                
    def Requestoken(self, data):
        self.sent_tokens = sent_tokenize(data)        
        pass
    
            
    def generate_Response(self,user_response):
        self.robo_response=''
        user_response=user_response.lower()
        self.word_tokens=self.word_tokens+nltk.word_tokenize(user_response)
        self.final_words=list(set(self.word_tokens))
        TfidfVec = TfidfVectorizer(tokenizer=self.LemNormalize, stop_words='english')
        tfidf = TfidfVec.fit_transform(self.sent_tokens)
        vals = cosine_similarity(tfidf[-1], tfidf)
        idx=vals.argsort()[0][-2]
        flat = vals.flatten()
        flat.sort()
        req_tfidf = flat[-2]
        if(req_tfidf==0):
            self.robo_response= self.robo_response+"I am sorry! I don't understand you"
        else:
            self.robo_response = self.robo_response+sent_tokens[idx]        
        return self.robo_response
        
    
    def aiResponse(self, data ):
        return( data)
    
        
    def open_file_js(self):
        with open("/var/www/app/data.json") as f:
            self.raw = json.load(f)
        
    
    def close_file(self):
        a_file = open("/var/www/app/data.json", "w")
        json.dump(self.raw, a_file)
        a_file.close()
    

       
