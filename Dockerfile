# base image
FROM python:3.7
# updating repository
RUN apt-get upgrade
RUN pip install --upgrade pip
# installing pip packages
RUN pip install --upgrade pip
RUN pip install tweepy
RUN pip install pandas
RUN pip install numpy
RUN pip install scipy
RUN pip install pyyaml
RUN pip install nltk
RUN pip install telepot
RUN pip install scikit-learn
RUN pip install PyMySQL
# setting up working application
WORKDIR /var/www/app
ENTRYPOINT ["python"]
CMD ["/var/www/app/app.py"]
